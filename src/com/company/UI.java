package com.company;

import com.company.dao.CSV;
import com.company.dao.JSON;
import com.company.dao.XML;
import com.company.dao.YAML;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class UI {
    Scanner scanner = new Scanner(System.in);

    private int numberOfSelectedFile;
    private File selectedFile;

    public void workWithFiles() {

        boolean tag1 = true;
        while (tag1) {
            System.out.println("Выберите желаемый формат для дальнейшей работы.");
            System.out.println("1.JSON\n2.CSV\n3.YAML\n4.XML");
            try {
                numberOfSelectedFile = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException ex) {
                System.out.println("Ответ введен не корректно, попробуйте еще раз!\n (Подсказка: введите цифру желаемого формата)");
                continue;
            }

            switch (numberOfSelectedFile) {
                case 1:
                    selectedFile = new File("persons.json");
                    tag1 = false;
                    break;
                case 2:
                    selectedFile = new File("persons.csv");
                    tag1 = false;
                    break;
                case 3:
                    selectedFile = new File("persons.yaml");
                    tag1 = false;
                    break;
                case 4:
                    selectedFile = new File("persons.xml");
                    tag1 = false;
                    break;
                default:
                    System.out.println("Ответ введен не корректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого формата)");
            }
        }

        boolean tag = true;

        while (tag) {
            System.out.println("Выберите действие:\n1.Записать нового пользователя\n2.Показать всех имеющихся пользователей\n3.Изменить пользователя\n4.Удалить пользователя\n5.Вспомогательные функции\n6.Завершить работу программы");
            int action;
            try {
                action = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException ex) {
                System.out.println("Ответ введен не корректно!\n(Подсказка: введите цифру напротив желаемого действия.)");
                continue;
            }

            switch (action) {
                case 1:
                    boolean tagAddPerson = true;
                    while (tagAddPerson) {
                        System.out.println("Имя");
                        String firstName = scanner.nextLine();
                        System.out.println("Фамилия");
                        String secondName = scanner.nextLine();
                        System.out.println("Возраст");
                        int age;
                        try {
                            age = Integer.parseInt(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println("Возраст введен не корректно, попробуйте еще раз!\n(Подсказка: введите число.) ");
                            continue;
                        }
                        System.out.println("Город");
                        String city = scanner.nextLine();
                        switch (numberOfSelectedFile) {
                            case 1:
                                JSON.writeToJSON(new Person(JSON.createID(), firstName, secondName, age, city));
                                break;
                            case 2:
                                CSV.writeToCSV(new Person(CSV.createID(), firstName, secondName, age, city));
                                break;
                            case 3:
                                YAML.writerYAML(new Person(YAML.createID(), firstName, secondName, age, city));
                                break;
                            case 4:
                                XML.writerXML(new Person(XML.createID(), firstName, secondName, age, city));
                                break;
                        }
                        System.out.println("Запись пользователя (" + firstName + " " + secondName + ", возраст " + age + ", город " + city + ") успешно завершена");
                        while (true) {
                            System.out.println("Желаете добавить еще одного пользователя?\n1.Да\n2.Нет");
                            int answerToContinue;
                            try {
                                answerToContinue = Integer.parseInt(scanner.nextLine());

                            } catch (NumberFormatException e) {
                                System.out.println("Ответ введен не коректно, попробуйте ще раз.\n(Подсказка: введите цифру желаемого ответа)");
                                continue;
                            }
                            if (answerToContinue == 2) {
                                tagAddPerson = false;
                                break;
                            } else if (answerToContinue == 1) {
                                break;
                            } else {
                                System.out.println("Ответ введен не коректно, попробуйте ще раз.\n(Подсказка: введите цифру напротив желаемого ответа");
                                continue;
                            }
                        }
                    }
                    break;

                case 2:
                    if (selectedFile.length() != 0) {

                        System.out.println("Все существующие пользователи в файле:");

                        switch (numberOfSelectedFile) {
                            case 1:
                                System.out.println(JSON.displayAll());
                                break;
                            case 2:
                                System.out.println(CSV.displayAll());
                                break;
                            case 3:
                                System.out.println(YAML.displayAll());
                                break;
                            case 4:
                                System.out.println(XML.displayAll());
                                break;
                        }
                    } else {
                        System.out.println("Данный файл пуст!");
                        break;
                    }
                    break;

                case 3:
                    if (selectedFile.length() != 0) {
                        int id;

                        switch (numberOfSelectedFile) {
                            case 1:
                                System.out.println(JSON.displayAll());
                                while (true) {
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        id = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\n(Подсказка: введите число.) ");
                                    }
                                }

                                if (JSON.checkId(id) == true) {
                                    int choose;
                                    while (true) {
                                        System.out.println("Выберете пункт который хотите изменить:\n1.Имя\n2.Фамилия\n3.Возраст\n4.Город");
                                        try {
                                            choose = Integer.parseInt(scanner.nextLine());
                                            if (choose >= 1 && choose <= 4) {
                                                break;
                                            } else {
                                                System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.) ");
                                                continue;
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.)");
                                        }
                                    }
                                    System.out.println("Введите изменяемую информацию");
                                    String info = scanner.nextLine();
                                    JSON.updatePerson(id, choose, info);
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 2:
                                System.out.println(CSV.displayAll());
                                while (true) {
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        id = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (CSV.checkId(id) == true) {
                                    int choose;
                                    while (true) {
                                        System.out.println("Выберете пункт который хотите изменить:\n1.Имя\n2.Фамилия\n3.Возраст\n4.Город");
                                        try {
                                            choose = Integer.parseInt(scanner.nextLine());
                                            if (choose >= 1 && choose <= 4) {
                                                break;
                                            } else {
                                                System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.) ");
                                                continue;
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.)");
                                        }
                                    }
                                    System.out.println("Введите изменяемую информацию");
                                    String info = scanner.nextLine();
                                    CSV.updatePerson(id, choose, info);
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 3:
                                System.out.println(YAML.displayAll());
                                while (true) {
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        id = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (YAML.checkId(id) == true) {
                                    int choose;
                                    while (true) {
                                        System.out.println("Выберете пункт который хотите изменить:\n1.Имя\n2.Фамилия\n3.Возраст\n4.Город");
                                        try {
                                            choose = Integer.parseInt(scanner.nextLine());
                                            if (choose >= 1 && choose <= 4) {
                                                break;
                                            } else {
                                                System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.) ");
                                                continue;
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.)");
                                        }
                                    }
                                    System.out.println("Введите изменяемую информацию");
                                    String info = scanner.nextLine();
                                    YAML.updatePerson(id, choose, info);
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 4:
                                System.out.println(XML.displayAll());
                                while (true) {
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        id = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (XML.checkId(id) == true) {
                                    int choose;
                                    while (true) {
                                        System.out.println("Выберете пункт который хотите изменить:\n1.Имя\n2.Фамилия\n3.Возраст\n4.Город");
                                        try {
                                            choose = Integer.parseInt(scanner.nextLine());
                                            if (choose >= 1 && choose <= 4) {
                                                break;
                                            } else {
                                                System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.) ");
                                                continue;
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("Ответ введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого пункта.)");
                                        }
                                    }
                                    System.out.println("Введите изменяемую информацию");
                                    String info = scanner.nextLine();
                                    XML.updatePerson(id, choose, info);
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                        }
                    } else {
                        System.out.println("Вы не можете изменить пустой файл!");
                    }
                    break;

                case 4:

                    if (selectedFile.length() != 0) {
                        int selectedId;

                        switch (numberOfSelectedFile) {
                            case 1:
                                while (true) {
                                    System.out.println("Cписок всех пользователей:");
                                    System.out.println(JSON.displayAll());
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        selectedId = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (JSON.checkId(selectedId) == true) {
                                    JSON.deletePerson(selectedId);
                                    System.out.println("Удаление пользователя успешно завершено!");
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 2:
                                while (true) {
                                    System.out.println("Cписок всех пользователей:");
                                    System.out.println(CSV.displayAll());
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        selectedId = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (CSV.checkId(selectedId) == true) {
                                    CSV.deletePerson(selectedId);
                                    System.out.println("Удаление пользователя успешно завершено!");
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 3:
                                while (true) {
                                    System.out.println("Cписок всех пользователей:");
                                    System.out.println(YAML.displayAll());
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        selectedId = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (YAML.checkId(selectedId) == true) {
                                    YAML.deletePerson(selectedId);
                                    System.out.println("Удаление пользователя успешно завершено!");
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;
                            case 4:
                                while (true) {
                                    System.out.println("Cписок всех пользователей:");
                                    System.out.println(XML.displayAll());
                                    System.out.println("Введите id пользователя, которого хотите изменить: ");
                                    try {
                                        selectedId = Integer.parseInt(scanner.nextLine());
                                        break;
                                    } catch (NumberFormatException e) {
                                        System.out.println("Id введен не корректно, попробуйте еще раз!\\n(Подсказка: введите число.) ");
                                    }
                                }
                                if (XML.checkId(selectedId) == true) {
                                    XML.deletePerson(selectedId);
                                    System.out.println("Удаление пользователя успешно завершено!");
                                } else {
                                    System.out.println("В базе данных нет пользователей с указанным id.");
                                }
                                break;

                        }
                    } else {
                        System.out.println("Вы не можете удалить пользователя в пустом файле!");
                    }
                    break;

                case 5:

                    if (selectedFile.length() != 0) {
                        ArrayList<Person> list = null;
                        int selectedHelper;
                        System.out.println("Выберите желаемую функцию:\n1.Найти пользователя по id\n2.Найти всех пользователей по имени\n3.Найти всех пользователей по фамилии\n4.Найти всех пользователей по возрасту\n5.Найти всех пользователей по городу\n6.Удалить всех пользователей");
                        switch (numberOfSelectedFile) {
                            case 1:
                                list = JSON.readFromJSON();
                                break;
                            case 2:
                                list = CSV.readFromCSV();
                                break;
                            case 3:
                                list = YAML.readFromYAML();
                                break;
                            case 4:
                                list = XML.readFromXML();
                                break;
                        }
                        try {
                            selectedHelper = Integer.parseInt(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            System.out.println("Ответ введен не корректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемой функции.)");
                            break;
                        }
                        workWithHelpers(selectedHelper, list);
                    } else {
                        System.out.println("Файл пустой, вы не можете использовать вспомогательные функции!");
                    }
                    break;
                case 6:
                    tag = false;
                    break;
                default:
                    System.out.println("Ответ введен не корректно, попробуйте еще раз!\n(Подсказка: введите цифру желаемого действия.)");

            }
        }
    }

    public void workWithHelpers(int select, ArrayList<Person> list) {
        switch (select) {
            case 1:
                int selectedId;
                while (true) {
                    System.out.println("Введите id пользователя");
                    try {
                        selectedId = Integer.parseInt(scanner.nextLine());
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Id пользователя введен не коректно, попробуйте еще раз!\n(Подсказка: введите цифру.)");
                    }
                }
                System.out.println(Helpers.getById(selectedId, list));
                break;

            case 2:
                System.out.println("Введите имя");
                String firstName = scanner.nextLine();
                System.out.println(Helpers.getAllByFirstName(firstName, list));
                break;
            case 3:
                System.out.println("Введите фамилию");
                String secondName = scanner.nextLine();
                System.out.println(Helpers.getAllByLastName(secondName, list));
                break;
            case 4:
                System.out.println("Введите возраст");
                int selectedAge = Integer.parseInt(scanner.nextLine());
                System.out.println(Helpers.getAllByAge(selectedAge, list));
                break;
            case 5:
                System.out.println("Введите город");
                String selectedCity = scanner.nextLine();
                System.out.println(Helpers.getAllByCity(selectedCity, list));
                break;
            case 6:
                int selectedSurable;
                while (true) {
                    System.out.println("Вы уверены?\n1.Да\n2.Нет");
                    try {
                        selectedSurable = Integer.parseInt(scanner.nextLine());
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("Ответ введен не корректно, попробуйте еще раз!\n(Подсказка: введите цифру желаевого ответа)");
                    }
                }
                if (selectedSurable == 1) {
                    Helpers.deleteAll(selectedFile);
                    break;
                } else {
                    break;
                }
        }
    }

//    public static void menu() throws IOException {
//
//
//        Scanner scanner = new Scanner(System.in);
//        boolean tag = false;
//        boolean menuTag = true;
//        boolean taghepls = false;
//
//        boolean writeTagJson = true;
//        boolean updateTagJson = true;
//        boolean deleteTagJSON = true;
//
//        boolean updateTagCsv = true;
//        boolean writeTagCsv = true;
//
//        boolean deleteXmlTag=true;
//        boolean updateTagXml = true;
//        boolean writeTagXml = true;
//
//        boolean updateTagYaml = true;
//        boolean writeTagYaml = true;
//
//
//
//
//        int selectCommandNum = 0;
//        int helpCommandNum = 0;
//        File json = new File("persons.json");
//        File csv = new File("persons.csv");
//        File xml = new File("persons.xml");
//        File yaml = new File("persons.yaml");
//
//
//        System.out.println("Введите цифру для выбора нужного формата хранения:");
//        System.out.println("1. JSON");
//        System.out.println("2. CSV");
//        System.out.println("3. XML");
//        System.out.println("4. YAML");
//
//        int choice = scanner.nextInt();
//        while (tag == false) {
//            switch (choice) {
//                case 1:
//                    System.out.println("Вы выбрали  JSON");
//                    tag = true;
//                    break;
//                case 2:
//                    System.out.println("Вы выбрали  CSV");
//                    tag = true;
//                    break;
//                case 3:
//                    System.out.println("Вы выбрали  XML");
//                    tag = true;
//                    break;
//                case 4:
//                    System.out.println("Вы выбрали  YAML");
//                    tag = true;
//                    break;
//                default:
//                    System.out.println("Ошибка ввода!Введите число от 1 до 4");
//                    choice = scanner.nextInt();
//            }
//            continue;
//        }
//
//        while (menuTag == true) {
//            Scanner scannerCommand = new Scanner(System.in);
//            System.out.println("Введите нужную команду");
//            System.out.println("1. Создать");
//            System.out.println("2. Читать");
//            System.out.println("3. Изменить");
//            System.out.println("4. Удалить");
//            System.out.println("5. Доп.функции");
//            System.out.println("6. Выход");
//
//
//            int selectCommand = Integer.parseInt(scannerCommand.nextLine());
//
//            switch (selectCommand) {
//                case 1:
//                    System.out.println("Создаем файл");
//                    selectCommandNum = 1;
//                    break;
//                case 2:
//                    System.out.println("Читаем файл");
//                    selectCommandNum = 2;
//                    break;
//                case 3:
//                    System.out.println("Редактируем");
//                    selectCommandNum = 3;
//                    break;
//                case 4:
//                    System.out.println("Удаляем файл");
//                    selectCommandNum = 4;
//                    break;
//                case 5:
//                    System.out.println("Открываем вспомогательные команды");
//                    taghepls = true;
//                    break;
//                case 6:
//                    System.exit(0);
//                    break;
//
//                default:
//                    System.out.println("Ошибка ввода! Введите команду");
//            }
//
//            if (selectCommandNum == 1 && choice == 1) while (writeTagJson == true) {
//
//                System.out.println("Введите имя");
//                String nameUser = scannerCommand.nextLine();
//                System.out.println("Введите фамилию");
//                String surnameUser = scannerCommand.nextLine();
//                System.out.println("Введите возраст");
//                int age = Integer.parseInt(scannerCommand.nextLine());
//
//                System.out.println("Введите город");
//                String city = scannerCommand.nextLine();
//                JSON.writeToJSON(new Person(JSON.createID(), nameUser, surnameUser, age, city));
//                System.out.println("Запись успешно создана:" + " " + nameUser + " " + surnameUser + ", возраст " + age + ", город " + city + ".");
//
//                writeTagJson = false;
//                System.out.println("Введите цифру с желаемым действием:" + "\n" + "1.Добавить пользователя" + "\n" + "2.Завершения работы программы"  + "\n" + "3.Перехода в меню");
//                Scanner scannerContComm = new Scanner(System.in);
//                int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                if(contCommand >=1 || contCommand <= 3) {
//                    if (contCommand == 1)
//                        writeTagJson = true;
//                    if (contCommand == 2)
//                        System.exit(0);
//                    if (contCommand == 3)
//                        break;
//                }else{
//                    System.out.println("Не коректно введена команда! Попробуйте еще раз.");
//                    contCommand = Integer.parseInt(scannerContComm.nextLine());
//                }
//            }
//
//
//            if (choice == 1 && selectCommandNum == 2) {
//                if (JSON.fileJSON.length() != 0) {
//                    System.out.println(JSON.displayAll());
//                } else System.out.println("Пустой файл.Сначала запиши данные");
//            }
//
//            if (choice == 1 && selectCommandNum == 3)
//                while (updateTagJson == true) {
//                    if (JSON.fileJSON.length() != 0) {
//                        System.out.println(JSON.displayAll());
//                        System.out.println("Выберите id записи которую хотите изменить");
//                        int selectId = Integer.parseInt(scannerCommand.nextLine());
//                        if(Helpers.getById(selectId, JSON.readFromJSON()) != "В базе данных нет пользователей с указанным id."){
//                        System.out.println("Выберите пункт записи который хотите изменить");
//                        System.out.println("1. Имя\n2. Фамилия\n3. Возраст\n4.Город");
//
//                        int selectUnit = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Измените поле ");
//                        String correctionText = scannerCommand.nextLine();
//                        JSON.updatePerson(selectId, selectUnit, correctionText);
//                        }else{
//                            System.out.println("В базе данных нет пользователей с указанным id.");
//                        }
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                        break;
//                    }
//                    Scanner scannerContComm = new Scanner(System.in);
//                    System.out.println("Введите цифру с желаемым действием:" + "\n" + "1.Изменить другого  пользователя" + "\n" + "2.Завершения работы программы"  + "\n" + "3.Перехода в меню");
//                    int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                    if (contCommand == 1)
//                        updateTagJson = true;
//                    if (contCommand == 2)
//                        System.exit(0);
//                    if (contCommand == 3)
//                        break;
//                }
//            if (choice == 1 && selectCommandNum == 4) {
//                while (deleteTagJSON == true) {
//                    if (json.length() != 0) {
//                        System.out.println(JSON.displayAll());
//                        Scanner scannerIdForDeleteJson = new Scanner(System.in);
//                        System.out.println("Введите id для удаления записи");
//                        int idForDeleteJson = Integer.parseInt(scannerIdForDeleteJson.nextLine());
//                        if(Helpers.getById(idForDeleteJson, JSON.readFromJSON()) != "В базе данных нет пользователей с указанным id."){
//                        JSON.deletePerson(idForDeleteJson);
//                        System.out.println("Запись удалена");
//                        }else{
//                            System.out.println("В базе данных нет пользователей с указанным id.");
//                        }
//                        deleteTagJSON = false;
//                        System.out.println("Введите цифру с желаемым действием:" + "\n" + "1.Удалить другого  пользователя" + "\n" + "2.Завершения работы программы"  + "\n" + "3.Перехода в меню");
//                        Scanner scannerContComm = new Scanner(System.in);
//                        int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                        if (contCommand == 1)
//                            deleteTagJSON = true;
//                        if (contCommand == 2)
//                            System.exit(0);
//                        if (contCommand == 3)
//                            menuTag = true;
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                        break;
//                    }
//
//                }
//
//            }
//
//
//            if (choice == 2 && selectCommandNum == 1)
//                while (writeTagCsv == true) {
//                    System.out.println("Введите имя");
//                    String nameUser = scannerCommand.nextLine();
//                    System.out.println("Введите фамилию");
//                    String surnameUser = scannerCommand.nextLine();
//                    System.out.println("Введите возраст");
//                    int age = Integer.parseInt(scannerCommand.nextLine());
//                    System.out.println("Введите город");
//                    String city = scannerCommand.nextLine();
//                    CSV.writeToCSV(new Person(CSV.createID(), nameUser, surnameUser, age, city));
//                    System.out.println("Запись успешно создана:" + " " + nameUser + " " + surnameUser + ", возраст " + age + ", город " + city + ".");
//                    writeTagCsv = false;
//                    Scanner scannerContCsv = new Scanner(System.in);
//                    System.out.println("Введите цифру с желаемого действия:" + "\n" + "1.Добавить нового пользователя" + "\n" + "2.Завершения работы программы"  + "\n" + "3.Перехода в меню");
//                    int contCommand = Integer.parseInt(scannerContCsv.nextLine());
//                    if (contCommand == 1)
//                        writeTagCsv = true;
//                    if (contCommand == 2)
//                        System.exit(0);
//                    if (contCommand == 3)
//                        break;
//
//                }
//            if (choice == 2 && selectCommandNum == 2)
//                if (csv.length() != 0) {
//                    System.out.println(CSV.displayAll());
//                } else {
//                    System.out.println("Файл пустой.Сначала введите данные");
//                }
//            if (choice == 2 && selectCommandNum == 3)
//                while (updateTagCsv == true) {
//                    if (csv.length() != 0) {
//                        System.out.println("Выберите id записи которую хотите изменить");
//                        int selectId = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Выберите пункт записи который хотите изменить");
//                        System.out.println("1. Имя\n2. Фамилия\n3. Возраст\n4.Город");
//                        int selectUnit = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Измените поле ");
//                        String correctionText = scannerCommand.nextLine();
//
//                        CSV.updatePerson(selectId, selectUnit, correctionText);
//                        Scanner scannerContComm = new Scanner(System.in);
//                        System.out.println("введите 1 чтобы изменить еще или 2 для выхода 3 в меню");
//                        int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                        if (contCommand == 1)
//                            updateTagCsv = true;
//                        if (contCommand == 2)
//                            System.exit(0);
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    break;
//                }
//
//            if (choice == 2 && selectCommandNum == 4) {
//                if (csv.length() != 0) {
//                    Scanner scannerIdForDelete = new Scanner(System.in);
//                    System.out.println("Введите id для удаления записи");
//                    int idForDelete = Integer.parseInt(scannerIdForDelete.nextLine());
//                    CSV.deletePerson(idForDelete);
//                    System.out.println("Запись удалена");
//                } else {
//                    System.out.println("Файл пустой.Сначала введите данные");
//                }
//            }
//
//
//            if (choice == 3 && selectCommandNum == 1)
//                while (writeTagXml == true) {
//                    System.out.println("Введите имя");
//                    String nameUser = scannerCommand.nextLine();
//                    System.out.println("Введите фамилию");
//                    String surnameUser = scannerCommand.nextLine();
//                    System.out.println("Введите возраст");
//                    int age = Integer.parseInt(scannerCommand.nextLine());
//                    System.out.println("Введите город");
//                    String city = scannerCommand.nextLine();
//                    XML.writerXML(new Person(XML.createID(), nameUser, surnameUser, age, city));
//                    System.out.println("Запись успешно создана:" + " " + nameUser + " " + surnameUser + ", " + age + " years, " + city + " city.");
//                    writeTagXml = false;
//                    Scanner scannerContCommXml = new Scanner(System.in);
//                    System.out.println("введите 1 чтобы изменить еще или 2 для выхода или 3 в меню");
//                    int contCommandXml = Integer.parseInt(scannerContCommXml.nextLine());
//                    if (contCommandXml == 1)
//                        writeTagXml = true;
//                    if (contCommandXml == 2)
//                        System.exit(0);
//                    if (contCommandXml == 3)
//                        break;
//                }
//
//            if (choice == 3 && selectCommandNum == 2) {
//                if (xml.length() != 0) {
//                    System.out.println(XML.readFromXML());
//                    System.out.println("введите 1 в меню или 2 для выхода");
//                    Scanner scannerContCommXml2 = new Scanner(System.in);
//                    int contCommand2 = Integer.parseInt(scannerContCommXml2.nextLine());
//                    if (contCommand2 == 1)
//                        menuTag = true;
//                    if (contCommand2 == 2)
//                        System.exit(0);
//                    if (contCommand2 == 3)
//                        break;
//                } else {
//                    System.out.println("Файл пустой.Сначала введите данные");
//                }
//            }
//
//
//            if (choice == 3 && selectCommandNum == 3) {
//                while (updateTagXml == true) {
//                    if (xml.length() != 0) {
//                        System.out.println("Выберите id записи которую хотите изменить");
//                        int selectId = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Выберите пункт записи который хотите изменить");
//                        System.out.println("1. Имя\n2. Фамилия\n3. Возраст\n4.Город");
//                        int selectUnit = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Измените поле ");
//                        String correctionText = scannerCommand.nextLine();
//
//                        XML.updatePerson(selectId, selectUnit, correctionText);
//                        Scanner scannerContComm = new Scanner(System.in);
//                        updateTagXml = false;
//                        System.out.println("введите 1 чтобы изменить еще или 2 для выхода ");
//                        int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                        if (contCommand == 1)
//                            updateTagXml = true;
//                        if (contCommand == 2)
//                            System.exit(0);
//                        if (contCommand == 3)
//                            menuTag = true;
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    break;
//                }
//
//            }
//            if (choice == 3 && selectCommandNum == 4) {
//                while (deleteXmlTag == true) {
//                    if (xml.length() != 0) {
//                        Scanner scannerIdForDeleteXml = new Scanner(System.in);
//                        System.out.println("Введите id для удаления записи");
//                        int idForDeleteXml = Integer.parseInt(scannerIdForDeleteXml.nextLine());
//                        XML.deletePerson(idForDeleteXml);
//                        deleteXmlTag = false;
//                        System.out.println("Запись удалена");
//                        System.out.println("введите 1 чтобы удалить еще или 2 для выхода или 3 в меню");
//                        Scanner scannerContComm = new Scanner(System.in);
//                        int contCommand = Integer.parseInt(scannerContComm.nextLine());
//                        if (contCommand == 1)
//                            deleteXmlTag = true;
//                        if (contCommand == 2)
//                            System.exit(0);
//                        if (contCommand == 3)
//                            menuTag = true;
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    break;
//                }
//            }
//
//
//            if (choice == 4 && selectCommandNum == 1)
//                while (writeTagYaml == true) {
//                    System.out.println("Введите имя");
//                    String nameUser = scannerCommand.nextLine();
//                    System.out.println("Введите фамилию");
//                    String surnameUser = scannerCommand.nextLine();
//                    System.out.println("Введите возраст");
//                    int age = Integer.parseInt(scannerCommand.nextLine());
//                    System.out.println("Введите город");
//                    String city = scannerCommand.nextLine();
//                    YAML.writerYAML(new Person(YAML.createID(), nameUser, surnameUser, age, city));
//                    System.out.println("Запись успешно создана:" + " " + nameUser + " " + surnameUser + ", " + age + " years, " + city + " city.");
//                    writeTagYaml = false;
//                    Scanner scannerContCommYaml = new Scanner(System.in);
//                    System.out.println("введите 1 чтобы изменить еще или 2 для выхода или  3  меню");
//                    int contCommandYaml = Integer.parseInt(scannerContCommYaml.nextLine());
//                    if (contCommandYaml == 1)
//                        writeTagYaml = true;
//                    if (contCommandYaml == 2)
//                        System.exit(0);
//                    if (contCommandYaml == 3)
//                        break;
//                }
//
//            if (choice == 4 && selectCommandNum == 2) {
//                while (menuTag == true) {
//                    if (yaml.length() != 0) {
//                        System.out.println(YAML.displayAll());
//                        System.out.println("введите 2 для выхода 3 в меню");
//                        menuTag = false;
//                        Scanner scannerContCommXml2 = new Scanner(System.in);
//                        int contCommandxml = Integer.parseInt(scannerContCommXml2.nextLine());
//                        if (contCommandxml == 1)
//                            updateTagXml = true;
//                        if (contCommandxml == 2)
//                            System.exit(0);
//                        if (contCommandxml == 3)
//
//                            menuTag = true;
//                        break;
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                }
//            }
//
//            if (choice == 4 && selectCommandNum == 3)
//                while (updateTagYaml == true) {
//                    if (yaml.length() != 0) {
//                        System.out.println("Выберите id записи которую хотите изменить");
//                        int selectId = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Выберите пункт записи который хотите изменить");
//                        System.out.println("1. Имя\n2. Фамилия\n3. Возраст\n4.Город");
//                        int selectUnit = Integer.parseInt(scannerCommand.nextLine());
//                        System.out.println("Измените поле ");
//                        String correctionText = scannerCommand.nextLine();
//
//                        YAML.updatePerson(selectId, selectUnit, correctionText);
//                        Scanner scannerContCommYaml = new Scanner(System.in);
//                        updateTagYaml = false;
//                        System.out.println("введите 1 чтобы изменить еще или 2 для выхода 3 меню");
//                        int contCommandYaml = Integer.parseInt(scannerContCommYaml.nextLine());
//                        if (contCommandYaml == 1)
//                            updateTagYaml = true;
//                        if (contCommandYaml == 2)
//                            System.exit(0);
//                        if (contCommandYaml == 3)
//                            menuTag = true;
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    break;
//                }
//
//            if (choice == 4 && selectCommandNum == 4) {
//                if (yaml.length() != 0) {
//                    Scanner scannerIdForDeleteYaml = new Scanner(System.in);
//                    System.out.println("Введите id для удаления записи");
//                    int idForDeleteYaml = Integer.parseInt(scannerIdForDeleteYaml.nextLine());
//                    YAML.deletePerson(idForDeleteYaml);
//                    System.out.println("Запись удалена");
//                } else {
//                    System.out.println("Файл пустой.Сначала введите данные");
//                }
//            }
//
//
//            while (taghepls == true) {
//                Scanner scannerHelpers = new Scanner(System.in);
//                System.out.println("Введите нужную команду");
//                System.out.println("1. Найти по id");
//                System.out.println("2. Найти по городу");
//                System.out.println("3. Найти по возрасту");
//                System.out.println("4. Найти по имени");
//                System.out.println("5. Найти по фамилии");
//                System.out.println("6. Удалить все");
//
//
//                int selectHelpers = Integer.parseInt(scannerHelpers.nextLine());
//                switch (selectHelpers) {
//                    case 1:
//                        System.out.println("Получаем по id");
//                        helpCommandNum = 1;
//                        taghepls = false;
//
//                        break;
//                    case 2:
//                        System.out.println("Получаем по городу");
//                        helpCommandNum = 2;
//                        taghepls = false;
//                        break;
//                    case 3:
//                        System.out.println("Получаем по возрасту");
//                        helpCommandNum = 3;
//                        taghepls = false;
//                        break;
//                    case 4:
//                        System.out.println("Получаем по имени");
//                        helpCommandNum = 4;
//                        taghepls = false;
//                        break;
//                    case 5:
//                        System.out.println("Получаем по фамилии");
//                        helpCommandNum = 5;
//                        taghepls = false;
//                        break;
//                    case 6:
//                        System.out.println("Удаляем весь файл");
//                        helpCommandNum = 6;
//                        taghepls = false;
//                        break;
//
//                    default:
//                        System.out.println("Вы не выбрали команду");
//
//                }
//
//
//                if (helpCommandNum == 1) {
//                    Scanner scannerCity = new Scanner(System.in);
//
//                    if (choice == 1) {
//                        if (json.length() != 0) {
//                            System.out.print("Введите ID: ");
//                            int scanId = Integer.parseInt(scannerCity.nextLine());
//                            System.out.println(Helpers.getById(scanId, JSON.readFromJSON()));
//                        } else {
//                            System.out.println("Файл пустой.Сначала введите данные");
//                        }
//                    }
//
//                    if (choice == 2) {
//                        if (csv.length() != 0) {
//                            System.out.println("Введите ID: ");
//                            int scanId = Integer.parseInt(scannerCity.nextLine());
//                            System.out.println(Helpers.getById(scanId, CSV.readFromCSV()));
//                        } else {
//                            System.out.println("Файл пустой.Сначала введите данные");
//                        }
//                    }
//
//                    if (choice == 3) {
//                        if (xml.length() != 0) {
//                            System.out.println("Введите ID: ");
//                            int scanId = Integer.parseInt(scannerCity.nextLine());
//                            System.out.println(Helpers.getById(scanId, XML.readFromXML()));
//                        } else {
//                            System.out.println("Файл пустой.Сначала введите данные");
//                        }
//                    }
//
//                    if (choice == 4) {
//                        if (yaml.length() != 0) {
//                            System.out.println("Введите ID: ");
//                            int scanId = Integer.parseInt(scannerCity.nextLine());
//                            System.out.println(Helpers.getById(scanId, YAML.readFromYAML()));
//                        } else {
//                            System.out.println("Файл пустой.Сначала введите данные");
//                        }
//                    }
//                    Scanner scannerAddition = new Scanner(System.in);
//                    System.out.println("Введите цифру с желаемого действия:" + "\n" + "1.Выбрать другую дополнительную команду" + "\n" + "2.Завершения работы программы"  + "\n" + "3.Перехода в меню");
//                    int com = Integer.parseInt(scannerAddition.nextLine());
//                    if (com == 1)
//                        taghepls = true;
//                    if (com == 2)
//                        System.exit(0);
//                    if (com == 3)
//                        menuTag = true;
//                    break;
//                }
//
//                if (helpCommandNum == 3) {
//                    Scanner scanner1 = new Scanner(System.in);
//
//
//                    if (choice == 1) {if (json.length() != 0) {System.out.println("введите возраст");
//                        int age1 = Integer.parseInt(scanner1.nextLine());
//                        System.out.println(Helpers.getAllByAge(age1, JSON.readFromJSON()));
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    }
//                    if (choice == 2) {if (csv.length() != 0) {System.out.println("введите возраст");
//                        int age1 = Integer.parseInt(scanner1.nextLine());
//                        System.out.println(Helpers.getAllByAge(age1, CSV.readFromCSV()));
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    }
//                    if (choice == 3) {if (xml.length() != 0) {System.out.println("введите возраст");
//                        int age1 = Integer.parseInt(scanner1.nextLine());
//                        System.out.println(Helpers.getAllByAge(age1, XML.readFromXML()));
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    }
//                    if (choice == 4) {if (yaml.length() != 0) {System.out.println("введите возраст");
//                        int age1 = Integer.parseInt(scanner1.nextLine());
//                        System.out.println(Helpers.getAllByAge(age1, YAML.readFromYAML()));
//                    } else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }
//                    }
////
//                    System.out.println("Хотите найти кого-то еще?");
//                    Scanner scannerAddition = new Scanner(System.in);
//                    System.out.println("введите 1 к командам или 2 для выхода 3 в меню");
//                    int com = Integer.parseInt(scannerAddition.nextLine());
//                    if (com == 1)
//                        taghepls = true;
//                    if (com == 2)
//                        System.exit(0);
//                    if (com == 3)
//                        menuTag=true;
//                    break;
//
//                }
//                if (helpCommandNum == 2) {
//                    Scanner scannerCity = new Scanner(System.in);
//
//                    if (choice == 1) {if (json.length() != 0){System.out.println("введите город");
//                        String scanCity = scannerCity.nextLine();
//                        System.out.println(Helpers.getAllByCity(scanCity, JSON.readFromJSON()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 2) {if (csv.length() != 0){System.out.println("введите город");
//                        String scanCity = scannerCity.nextLine();
//                        System.out.println(Helpers.getAllByCity(scanCity, CSV.readFromCSV()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 3) {if (xml.length() != 0){System.out.println("введите город");
//                        String scanCity = scannerCity.nextLine();
//                        System.out.println(Helpers.getAllByCity(scanCity, XML.readFromXML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 4) {if (yaml.length() != 0){System.out.println("введите город");
//                        String scanCity = scannerCity.nextLine();
//                        System.out.println(Helpers.getAllByCity(scanCity, YAML.readFromYAML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//
//                    System.out.println("Хотите найти кого-то еще?");
//                    Scanner scannerAddition = new Scanner(System.in);
//                    System.out.println("введите 1 чтобы найти кого-то еще или 2 для выхода 3 в меню");
//                    int com = Integer.parseInt(scannerAddition.nextLine());
//                    if (com == 1)
//                        taghepls = true;
//                    if (com == 2)
//                        System.exit(0);
//                    if (com == 3)
//                        menuTag=true;
//                    break;
//                }
//                if (helpCommandNum == 4) {
//                    Scanner scannerName = new Scanner(System.in);
//
//                    if (choice == 1) {if (json.length() != 0){System.out.println("введите имя");
//                        String scanName = scannerName.nextLine();
//                        System.out.println(Helpers.getAllByFirstName(scanName, JSON.readFromJSON()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 2) {if (csv.length() != 0){System.out.println("введите имя");
//                        String scanName = scannerName.nextLine();
//                        System.out.println(Helpers.getAllByFirstName(scanName, CSV.readFromCSV()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 3) {if (xml.length() != 0){System.out.println("введите имя");
//                        String scanName = scannerName.nextLine();
//                        System.out.println(Helpers.getAllByFirstName(scanName, XML.readFromXML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 4) {if (yaml.length() != 0){System.out.println("введите имя");
//                        String scanName = scannerName.nextLine();
//                        System.out.println(Helpers.getAllByFirstName(scanName, YAML.readFromYAML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//
//                    System.out.println("Хотите найти кого-то еще?");
//                    Scanner scannerAddition = new Scanner(System.in);
//                    System.out.println("введите 1 к командам или 2 для выхода 3 в меню");
//                    int com = Integer.parseInt(scannerAddition.nextLine());
//                    if (com == 1)
//                        taghepls = true;
//                    if (com == 2)
//                        System.exit(0);
//                    if (com == 3)
//                        menuTag=true;
//                    break;
//                }
//                if (helpCommandNum == 5) {
//                    Scanner scannerSurname = new Scanner(System.in);
//
//                    if (choice == 1) {if (json.length() != 0){System.out.println("введите фамилию");
//                        String scanSurname = scannerSurname.nextLine();
//                        System.out.println(Helpers.getAllByLastName(scanSurname, JSON.readFromJSON()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 2) {if (csv.length() != 0){System.out.println("введите фамилию");
//                        String scanSurname = scannerSurname.nextLine();
//                        System.out.println(Helpers.getAllByLastName(scanSurname, CSV.readFromCSV()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 3) {if (xml.length() != 0){System.out.println("введите фамилию");
//                        String scanSurname = scannerSurname.nextLine();
//                        System.out.println(Helpers.getAllByLastName(scanSurname, XML.readFromXML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//                    if (choice == 4) {if (yaml.length() != 0){System.out.println("введите фамилию");
//                        String scanSurname = scannerSurname.nextLine();
//                        System.out.println(Helpers.getAllByLastName(scanSurname, YAML.readFromYAML()));
//                    }else {
//                        System.out.println("Файл пустой.Сначала введите данные");
//                    }}
//
//                    System.out.println("Хотите найти кого-то еще?");
//                    Scanner scannerAddition = new Scanner(System.in);
//                    System.out.println("введите 1 к командам или 2 для выхода 3 в меню");
//                    int com = Integer.parseInt(scannerAddition.nextLine());
//                    if (com == 1)
//                        taghepls = true;
//                    if (com == 2)
//                        System.exit(0);
//                    if (com == 3)
//                        menuTag=true;
//                    break;
//
//
//                }
//                if (helpCommandNum == 6) {
//                    if (choice == 1) {
//                        Helpers.deleteAll(json);
//                    }
//                    if (choice == 2) {
//                        Helpers.deleteAll(csv);
//                    }
//                    if (choice == 3) {
//                        Helpers.deleteAll(xml);
//                    }
//                    if (choice == 4) {
//                        Helpers.deleteAll(yaml);
//                    }
//                }
//
//            }
//        }
//    }
}
