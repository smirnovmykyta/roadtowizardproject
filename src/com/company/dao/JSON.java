package com.company.dao;

import com.company.Helpers;
import com.company.Person;

import java.io.*;
import java.util.ArrayList;

public class JSON {

    public static File fileJSON = new File("persons.json");

    public static void writeToJSON(Person person) {
        try {

            FileWriter writer = new FileWriter(fileJSON, true);
            FileReader reader = new FileReader(fileJSON);

            BufferedReader bufferedReader = new BufferedReader(reader);


            if (reader.read() == -1) {

                String result = "[" + toStringJSON(person) + "]";
                writer.write(result);

                writer.close();
            } else {

                String result = bufferedReader.readLine();

                RandomAccessFile file = new RandomAccessFile(fileJSON, "rw");
                file.seek(result.lastIndexOf("]"));
                file.write("},".getBytes());

                file.write((toStringJSON(person) + "]").getBytes());
                file.close();

                bufferedReader.close();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Person> readFromJSON() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileJSON));
            String line = bufferedReader.readLine();
            String[] arrayPersons = line.split("},");
            for (String splitterPersons : arrayPersons) {

                int id = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("id") + 6, splitterPersons.indexOf(",")));

                String firstName = splitterPersons.substring(splitterPersons.indexOf("firstName") + 14, splitterPersons.indexOf("\",", splitterPersons.indexOf("firstName")));

                String lastName = splitterPersons.substring(splitterPersons.indexOf("lastName") + 13, splitterPersons.indexOf("\",", splitterPersons.indexOf("lastName")));

                int age = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("age") + 7, splitterPersons.indexOf(",", splitterPersons.indexOf("age"))));

                String city = splitterPersons.substring(splitterPersons.indexOf("city") + 9, splitterPersons.indexOf("\"", splitterPersons.indexOf("city") + 9));


                Person person = new Person(id, firstName, lastName, age, city);
                listOfPersons.add(person);
                bufferedReader.close();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfPersons;
    }

    public static void updatePerson(int id, int choose, String newInformation) {
        ArrayList<Person> personToUpdate = readFromJSON();
        System.out.println(Helpers.getById(id, personToUpdate) + "\nИзменен на:");

        for (Person splitterPersonBeforUpdate : personToUpdate) {
            if (splitterPersonBeforUpdate.getId() == id) {
                switch (choose) {
                    case 1:
                        splitterPersonBeforUpdate.setFirstName(newInformation);
                        break;
                    case 2:
                        splitterPersonBeforUpdate.setLastName(newInformation);
                        break;
                    case 3:
                        try {
                            splitterPersonBeforUpdate.setAge(Integer.parseInt(newInformation));
                        } catch (Exception e) {
                        }
                        break;
                    case 4:
                        splitterPersonBeforUpdate.setCity(newInformation);
                        break;
                }
                System.out.println(Helpers.getById(id, personToUpdate));
            }
        }
        try {
            FileWriter writer = new FileWriter(fileJSON);

            String result = "[";

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result += toStringJSON(splitterPersonAfterUpdate) + ",";
            }
            result = result.replaceAll(",$", "]");
            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void deletePerson(int id) {
        ArrayList<Person> personToDelete = readFromJSON();
        personToDelete.removeIf(person -> person.getId() == id);

        try {
            FileWriter writer = new FileWriter(fileJSON);

            String result = "[";

            for (Person splitterPersonAfterDelete : personToDelete) {
                result += toStringJSON(splitterPersonAfterDelete) + ",";
            }

            result = result.replaceAll(",$", "]");
            if (result == "[") {
                result = "";
            }

            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String displayAll() {
        ArrayList<Person> allPersons = readFromJSON();
        StringBuilder displayPerson = new StringBuilder();
        for (Person splitterPerson : allPersons) {
            displayPerson.append(splitterPerson.toString());
        }
        return displayPerson.toString();

    }

    public static int createID() {
        ArrayList<Person> personsList;
        try {
            personsList = readFromJSON();
        } catch (Exception e) {
            return 0;
        }

        return personsList.get(personsList.size() - 1).getId() + 1;
    }

    public static boolean checkId(int id) {
        ArrayList<Person> personsList = readFromJSON();
        for (Person splitterPerson : personsList) {
            if (splitterPerson.getId() == id) {
                return true;
            }
        }
        return false;
    }


    public static String toStringJSON(Person person) {
        return "{" +
                "\"id\" : " + person.getId() +
                ", \"firstName\" : " + '\"' + person.getFirstName() + '\"' +
                ", \"lastName\" : " + '\"' + person.getLastName() + '\"' +
                ", \"age\" : " + person.getAge() +
                ", \"city\" : " + '\"' + person.getCity() + '\"' +
                '}';
    }

}