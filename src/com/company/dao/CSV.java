package com.company.dao;

import com.company.Helpers;
import com.company.Person;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CSV {

    //    public static ArrayList<Person> list = new ArrayList<>();
    static File csvOutputFile = new File("persons.csv");

    public static void writeToCSV(Person person) {

        try {

            FileWriter fileWriter = new FileWriter(csvOutputFile, true);
            List<Person> personList = Arrays.asList(person);
            for (Person splitterPerson : personList) {
                fileWriter.write(toStringCSV(splitterPerson));
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }


    public static ArrayList<Person> readFromCSV() {
        ArrayList<Person> list = new ArrayList<>();

        try {
            File file = new File("persons.csv");
            String line = (Files.readAllLines(file.toPath())).toString();
            String[] arrayPersons = line.split(",  ");
            for (String splitterPersons : arrayPersons) {

                if (splitterPersons.equals("]")) {
                    break;
                }

                int id = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("id") + 3, splitterPersons.indexOf(",", splitterPersons.indexOf("id"))));


                String firstName = splitterPersons.substring(splitterPersons.indexOf("firstName") + 10, splitterPersons.indexOf(",", splitterPersons.indexOf("firstName")));


                String lastName = splitterPersons.substring(splitterPersons.indexOf("lastName") + 9, splitterPersons.indexOf(",", splitterPersons.indexOf("lastName")));

                int age = Integer.parseInt(splitterPersons.substring(splitterPersons.indexOf("age") + 4, splitterPersons.indexOf(",", splitterPersons.indexOf("age"))));

                String city = splitterPersons.substring(splitterPersons.indexOf("city") + 5);

                Person person = new Person(id, firstName, lastName, age, city);
                list.add(person);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static String toStringCSV(Person person) {
        return
                " id=" + person.getId() +
                        ", firstName=" + person.getFirstName() +
                        ", lastName=" + person.getLastName() +
                        ", age=" + person.getAge() +
                        ", city=" + person.getCity() + "\n";
    }


    public static void updatePerson(int id, int choose, String newInformation) {
        ArrayList<Person> personToUpdate = readFromCSV();
        System.out.println(Helpers.getById(id, personToUpdate) + "\nИзменен на:");

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                switch (choose) {
                    case 1:
                        splitterPersonBeforeUpdate.setFirstName(newInformation);
                        break;
                    case 2:
                        splitterPersonBeforeUpdate.setLastName(newInformation);
                        break;
                    case 3:
                        try {
                            splitterPersonBeforeUpdate.setAge(Integer.parseInt(newInformation));
                        } catch (Exception e) {
                        }
                        break;
                    case 4:
                        splitterPersonBeforeUpdate.setCity(newInformation);
                        break;
                }
                System.out.println(Helpers.getById(id, personToUpdate));
            }
        }


        try {
            FileWriter writer = new FileWriter(csvOutputFile);

            String result = "";

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result += toStringCSV(splitterPersonAfterUpdate);
            }
            result = result.replaceAll("]$", "");
            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deletePerson(int id) {
        ArrayList<Person> personToDelete = readFromCSV();
        personToDelete.removeIf(person -> person.getId() == id);
        try {
            FileWriter writer = new FileWriter(csvOutputFile);

            String result = "";

            for (Person splitterPersonAfterDelete : personToDelete) {
                result += toStringCSV(splitterPersonAfterDelete);
            }
            result = result.replaceAll("]$", "");
            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String displayAll() {
        ArrayList<Person> allPersons = readFromCSV();
        StringBuilder displayPerson = new StringBuilder();
        for (Person splitterPersons : allPersons) {
            displayPerson.append(splitterPersons.toString());
        }
        return displayPerson.toString().replaceAll("]", "");

    }

    public static int createID() {
        ArrayList<Person> personsList;
        try {
            personsList = readFromCSV();
        } catch (Exception e) {
            return 0;
        }

        return personsList.get(personsList.size() - 1).getId() + 1;
    }

    public static boolean checkId(int id) {
        ArrayList<Person> personsList = readFromCSV();
        for (Person splitterPersons : personsList) {
            if (splitterPersons.getId() == id) {
                return true;
            }
        }
        return false;
    }

}