package com.company.dao;

import com.company.Helpers;
import com.company.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.nio.file.Files;
import java.util.stream.Collectors;


public class YAML {

    //      public static  ArrayList<Person> list = new ArrayList<>();
    public static File file = new File("persons.yaml");


    public static void writerYAML(Person person) {
        try {
            FileWriter fileWriter = new FileWriter(file, true);

            List<Person> personList = Arrays.asList(person);
            for (Person splitterPersonList : personList) {
                fileWriter.write(writeYAMLСonstructor(splitterPersonList));
            }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static ArrayList<Person> readFromYAML() {
        ArrayList<Person> listOfPersons = new ArrayList<>();

        try {
            //BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line = (Files.readAllLines(file.toPath())).toString();

            String[] personList = line.split(", ,   ");

            for (String splitterPerson : personList) {
                if (splitterPerson.equals("]")) {
                    break;
                }

                int id = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("id") + 5, splitterPerson.indexOf(",", splitterPerson.indexOf("id"))));

                String firstName = splitterPerson.substring(splitterPerson.indexOf("first name") + 13, splitterPerson.indexOf(",", splitterPerson.indexOf("first name")));

                String lastName = splitterPerson.substring(splitterPerson.indexOf("last name") + 12, splitterPerson.indexOf(",", splitterPerson.indexOf("last name")));

                int age = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("age") + 6, splitterPerson.indexOf(",", splitterPerson.indexOf("age"))));

                String city = splitterPerson.substring(splitterPerson.indexOf("city") + 7);

                Person person = new Person(id, firstName, lastName, age, city);
                listOfPersons.add(person);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfPersons;
    }

    public static void updatePerson(int id, int choose, String newInformation) {
        ArrayList<Person> personToUpdate = readFromYAML();
        System.out.println(Helpers.getById(id, personToUpdate) + "\nИзменен на:");

        for (Person splitterPersonBeforUpdate : personToUpdate) {
            if (splitterPersonBeforUpdate.getId() == id) {
                switch (choose) {
                    case 1:
                        splitterPersonBeforUpdate.setFirstName(newInformation);
                        break;
                    case 2:
                        splitterPersonBeforUpdate.setLastName(newInformation);
                        break;
                    case 3:
                        try {
                            splitterPersonBeforUpdate.setAge(Integer.parseInt(newInformation));
                        } catch (Exception e) {
                        }
                        break;
                    case 4:
                        splitterPersonBeforUpdate.setCity(newInformation);
                        break;
                }
                System.out.println(Helpers.getById(id, personToUpdate));
            }
        }
        try {
            FileWriter writer = new FileWriter(file);

            String result = "";

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result += writeYAMLСonstructor(splitterPersonAfterUpdate) + ",";
            }
            result = result.replaceAll(",", "");
            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void deletePerson(int id) {
        ArrayList<Person> personToDelete = readFromYAML();
        personToDelete.removeIf(person -> person.getId() == id);

        try {
            FileWriter writer = new FileWriter(file);

            String result = "";

            for (Person splitterPersonAfterDelete : personToDelete) {
                result += writeYAMLСonstructor(splitterPersonAfterDelete);
            }


            writer.write(result);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String displayAll() {
        ArrayList<Person> allPersons = readFromYAML();
        StringBuilder displayPerson = new StringBuilder();
        for (Person splitterPerson : allPersons) {
            displayPerson.append(splitterPerson.toString());
        }
        return displayPerson.toString();

    }

    public static int createID() {
        ArrayList<Person> persons;
        try {
            persons = readFromYAML();
        } catch (Exception e) {
            return 0;
        }

        return persons.get(persons.size() - 1).getId() + 1;
    }

    public static boolean checkId(int id) {
        ArrayList<Person> persons = readFromYAML();
        for (Person splitterPerson : persons) {
            if (splitterPerson.getId() == id) {
                return true;
            }
        }
        return false;
    }


    public static String writeYAMLСonstructor(Person person) {

        String construct = "persona" + person.getId() + ":"
                + "\n\t - id : " + person.getId()
                + "\n\t - first name : " + person.getFirstName()
                + "\n\t - last name : " + person.getLastName()
                + "\n\t - age : " + person.getAge()
                + "\n\t - city : " + person.getCity() + "\n\n  ";

        return construct;
    }


}
