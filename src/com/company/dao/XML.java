package com.company.dao;

import com.company.Helpers;
import com.company.Person;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XML {

    public static File file = new File("persons.xml");

    public static void writerXML(Person person) {
        try {

            FileWriter fileWriter = new FileWriter(file, true);
            FileReader reader = new FileReader(file);

            if (reader.read() == -1) {

                String result = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "\n<Main>" + writeXMLСonstructor(person) + "</Main>";

                fileWriter.write(result);
                fileWriter.close();
            } else {
                ArrayList<Person> personToAdd = readFromXML();
                String result = "";

                for (Person splitterPerson : personToAdd) {
                    result += writeXMLСonstructor(splitterPerson);
                }

                result += writeXMLСonstructor(person) + "</Main>";
                result = result.replaceAll(" , 	</city>", "");
                PrintWriter writer = new PrintWriter(file);
                writer.print("");
                fileWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "\n<Main>" + result);
                fileWriter.close();
            }

        } catch (
                IOException e) {
            System.out.println(e);
        }

    }

    public static ArrayList<Person> readFromXML() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        try {
            String line = (Files.readAllLines(file.toPath())).toString();

            String[] arrayLine = line.split("</Person>");
            for (String splitterPerson : arrayLine) {
                if (splitterPerson.equals(", ,   </Main>]")) {
                    break;
                }

//                System.out.println(s);

                int id = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("id") + 3, splitterPerson.indexOf("</id>")));

                String firstName = splitterPerson.substring(splitterPerson.indexOf("first_name") + 11, splitterPerson.indexOf("</first_name>"));

                String lastName = splitterPerson.substring(splitterPerson.indexOf("second_name") + 12, splitterPerson.indexOf("</second_name>"));

                int age = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("age") + 4, splitterPerson.indexOf("</age>")));

                String city = splitterPerson.substring(splitterPerson.indexOf("city") + 5, splitterPerson.indexOf("</city>"));

                Person person = new Person(id, firstName, lastName, age, city);
                listOfPersons.add(person);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfPersons;
    }

    public static void updatePerson(int id, int choose, String newInformation) {
        ArrayList<Person> personToUpdate = readFromXML();
        System.out.println(Helpers.getById(id, personToUpdate) + "\nИзменен на:");

        for (Person splitterPersonBeforUpdate : personToUpdate) {
            if (splitterPersonBeforUpdate.getId() == id) {
                switch (choose) {
                    case 1:
                        splitterPersonBeforUpdate.setFirstName(newInformation);
                        break;
                    case 2:
                        splitterPersonBeforUpdate.setLastName(newInformation);
                        break;
                    case 3:
                        try {
                            splitterPersonBeforUpdate.setAge(Integer.parseInt(newInformation));
                        } catch (Exception e) {
                        }
                        break;
                    case 4:
                        splitterPersonBeforUpdate.setCity(newInformation);
                        break;
                }
                System.out.println(Helpers.getById(id, personToUpdate));
            }
        }
        try {
            FileWriter writer = new FileWriter(file);

            String result = "";

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result += writeXMLСonstructor(splitterPersonAfterUpdate) + ",";
            }
            result = result.replaceAll(",", "");
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "\n<Main>" + result + "</Main>");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void deletePerson(int id) {
        ArrayList<Person> personToDelete = readFromXML();
        personToDelete.removeIf(person -> person.getId() == id);

        try {
            FileWriter writer = new FileWriter(file);

            String result = "";

            for (Person splitterPersonAfterDelete : personToDelete) {
                result += writeXMLСonstructor(splitterPersonAfterDelete);
            }
            if (result == "") {
                writer.write(result);
            } else {
                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "\n<Main>" + result + "</Main>");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String displayAll() {
        ArrayList<Person> allPersons = readFromXML();
        StringBuilder displayPerson = new StringBuilder();
        for (Person splitterPerson : allPersons) {
            displayPerson.append(splitterPerson.toString());
        }
        return displayPerson.toString();

    }

    public static int createID() {
        ArrayList<Person> personsList;
        try {
            personsList = readFromXML();
        } catch (Exception e) {
            return 0;
        }

        return personsList.get(personsList.size() - 1).getId() + 1;
    }

    public static boolean checkId(int id) {
        ArrayList<Person> personsList = readFromXML();
        for (Person splitterPerson : personsList) {
            if (splitterPerson.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public static String writeXMLСonstructor(Person person) {

        String construct = "\n\t<Person>"
                + "\n\t <id>" + person.getId() + "</id>"
                + "\n\t <first_name>" + person.getFirstName() + "</first_name>"
                + "\n\t <second_name>" + person.getLastName() + "</second_name>"
                + "\n\t <age>" + person.getAge() + "</age>"
                + "\n\t <city>" + person.getCity() + "</city> " +
                "\n\t</Person>" + "\n\n  ";

        return construct;
    }
}
