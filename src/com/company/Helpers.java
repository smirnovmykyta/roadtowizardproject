package com.company;

import com.company.Person;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Helpers {

    public static String getById(int id, ArrayList<Person> persons) {

        String result = "";

        for (Person o : persons) {
            if (o.getId() == id) {
                result = o.getFirstName() + " " + o.getLastName() + ", возраст " + o.getAge() + ", город " + o.getCity() + ".";
            }
        }
        if (result.isEmpty()) {
            result = "В базе данных нет пользователей с указанным id.";
        }
        return result;
    }

    public static String getAllByCity(String city, ArrayList<Person> persons) {

        String result = "";

        for (Person o : persons) {

            if (o.getCity().equals(city)) {
                result += o.getFirstName() + " " + o.getLastName() + ", возраст " + o.getAge() + ";\n";
            }
        }
        if (result.isEmpty()) {
            result = "В базе данных нет пользователей с указанным городом.";
        }

        result = result.replaceAll(";$", ".");

        return result;
    }

    public static String getAllByAge(int age, ArrayList<Person> persons) {

        String result = "";

        for (Person o : persons) {

            if (o.getAge() == age) {
                result += o.getFirstName() + " " + o.getLastName() + ",город " + o.getCity() + ";\n";
            }
        }
        if (result.isEmpty()) {
            result = "В базе данных нет пользователей с указанным возрастом.";
        }

        result = result.replaceAll(";$", ".");

        return result;
    }


    public static String getAllByFirstName(String firstName, ArrayList<Person> persons) {

        String result = "";

        for (Person o : persons) {

            if (o.getFirstName().equals(firstName)) {
                result += o.getFirstName() + " " + o.getLastName() + ", возраст " + o.getAge() + ", город  " + o.getCity() + ";\n";
            }
        }

        if (result.isEmpty()) {
            result = "В базе данных нет пользователей с указанным именем.";
        }

        result = result.replaceAll(";$", ".");

        return result;
    }


    public static String getAllByLastName(String lastName, ArrayList<Person> persons) {

        String result = "";

        for (Person o : persons) {

            if (o.getLastName().equals(lastName)) {
                result += o.getFirstName() + " " + o.getLastName() + ", возраст " + o.getAge() + ", город " + o.getCity() + ";\n";
            }
        }

        if (result.isEmpty()) {
            result = "В базе данных нет пользователем с указанной фамилией.";
        }

        result = result.replaceAll(";$", ".");

        return result;
    }


    public static void deleteAll(File file) {
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Все пользователи удалены.");
    }

}

