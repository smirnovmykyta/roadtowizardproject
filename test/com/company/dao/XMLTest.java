package com.company.dao;

import com.company.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class XMLTest {

//    @Test
//    void writerXML() {
//    }
//
//    @Test
//    void readFromXML() {
//    }
//
//    @Test
//    void updatePerson() {
//    }
//
//    @Test
//    void deletePerson() {
//    }
//
//    @Test
//    void displayAll() {
//    }
//
//    @Test
//    void createID() {
//    }

    @Test
    void writeXMLСonstructor_ShouldWriteInFileInRightFormatPass() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "\n\t<Person>"
                + "\n\t <id>" + 1 + "</id>"
                + "\n\t <first_name>" + "Dariia" + "</first_name>"
                + "\n\t <second_name>" + "Kaptiurova" + "</second_name>"
                + "\n\t <age>" + 20 + "</age>"
                + "\n\t <city>" + "Kyiv" + "</city> " +
                "\n\t</Person>" + "\n\n  ";

        //when
        String actual = XML.writeXMLСonstructor(person);

        //then
        assertEquals(expected, actual);
    }

    @Test
    void writeXMLСonstructor_ShouldWriteInFileInRightFormatFailed() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "Dariia, Kaptiurova";

        //when
        String actual = XML.writeXMLСonstructor(person);

        //then
        assertNotEquals(expected, actual);
    }
}