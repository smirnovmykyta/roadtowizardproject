package com.company.dao;

import com.company.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class YAMLTest {

//    @Test
//    void writerYAML() {
//    }
//
//    @Test
//    void readFromYAML() {
//    }
//
//    @Test
//    void updatePerson() {
//    }
//
//    @Test
//    void deletePerson() {
//    }
//
//    @Test
//    void displayAll() {
//    }
//
//    @Test
//    void createID() {
//    }

    @Test
    void writeYAMLСonstructor_ShouldWriteInFileInRightFormatPass() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "persona" + 1 + ":"
                + "\n\t - id : " + 1
                + "\n\t - first name : " + "Dariia"
                + "\n\t - last name : " + "Kaptiurova"
                + "\n\t - age : " + 20
                + "\n\t - city : " + "Kyiv" + "\n\n  ";

        //when
        String actual = YAML.writeYAMLСonstructor(person);

        //then
        assertEquals(expected, actual);
    }

    @Test
    void writeYAMLСonstructor_ShouldWriteInFileInRightFormatFailed() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "Dariia, Kaptiurova";

        //when
        String actual = YAML.writeYAMLСonstructor(person);

        //then
        assertNotEquals(expected, actual);
    }
}
