package com.company.dao;

import com.company.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVTest {

    @Test
    void toStringCSV_ShouldWriteInFileInRightFormatPass() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected =
                " id=" + 1 +
                        ", firstName=" + "Dariia" +
                        ", lastName=" + "Kaptiurova" +
                        ", age=" + 20 +
                        ", city=" + "Kyiv" + "\n";

        //when
        String actual = CSV.toStringCSV(person);

        //then
        assertEquals(expected, actual);
    }

    @Test
    void toStringCSV_ShouldWriteInFileInRightFormatFailed() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "Dariia, Kaptiurova";

        //when
        String actual = CSV.toStringCSV(person);

        //then
        assertNotEquals(expected, actual);
    }

}