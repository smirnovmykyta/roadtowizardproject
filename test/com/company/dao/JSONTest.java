package com.company.dao;

import com.company.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JSONTest {

//    @Test
//    void writeToJSON() {
//    }
//
//    @Test
//    void readFromJSON() {
//    }
//
//    @Test
//    void updatePerson() {
//    }
//
//    @Test
//    void deletePerson() {
//    }
//
//    @Test
//    void displayAll() {
//    }
//
//    @Test
//    void createID() {
//    }

    @Test
    void toStringJSON_ShouldWriteInFileInRightFormatPass() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "{" +
                "\"id\" : " + 1 +
                ", \"firstName\" : " + '\"' + "Dariia" + '\"' +
                ", \"lastName\" : " + '\"' + "Kaptiurova" + '\"' +
                ", \"age\" : " + 20 +
                ", \"city\" : " + '\"' + "Kyiv" + '\"' +
                '}';

        //when
        String actual = JSON.toStringJSON(person);

        //then
        assertEquals(expected, actual);
    }

    @Test
    void toStringJSON_ShouldWriteInFileInRightFormatFailed() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "Dariia, Kaptiurova";

        //when
        String actual = JSON.toStringJSON(person);

        //then
        assertNotEquals(expected, actual);
    }
}