package com.company;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HelpersTest {

    ArrayList<Person> test = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        test.add(new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv"));
        test.add(new Person(2, "Nikita", "Smirnov", 22, "Kyiv"));
    }

    @Test
    void getById_ShouldReturnPersonByIDPass() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20, город Kyiv." + "\n";
        int id = 1;
        String expected1 = "В базе данных нет пользователей с указанным id.";
        int id1 = 10;

        //when
        String actual = Helpers.getById(id, test);
        String actual1 = Helpers.getById(id1, test);

        //then
//        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
    }


    @Test
    void getById_ShouldReturnPersonByIDFailed() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20, город Kyiv." + "\n";
        int id = 2;

        //when
        String actual = Helpers.getById(id, test);

        //then
        assertNotEquals(expected, actual);
    }


    @Test
    void getAllByCity_ShouldReturnPeopleByTheSameCityPass() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20; \n Nikita Smirnov, возраст 22." + "\n";
        String city = "Kyiv";
        String expected1 = "В базе данных нет пользователей с указанным городом.";
        String city1 = "Poltava";

        //when
        String actual = Helpers.getAllByCity(city, test);
        String actual1 = Helpers.getAllByCity(city1, test);

        //then
//        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
    }


    @Test
    void getAllByCity_ShouldReturnPeopleByTheSameCityFailed() {

        //given
        String expected = "Nikita Smirnov, возраст 22." + "\n";
        String city = "Kyiv";

        //when
        String actual = Helpers.getAllByCity(city, test);

        //then
        assertNotEquals(expected, actual);
    }


    @Test
    void getAllByAge_ShouldReturnPeopleByTheSameAgePass() {

        //given
        String expected = "Dariia Kaptiurova,город Kyiv." + "\n";
        int age = 20;
        String expected1 = "В базе данных нет пользователей с указанным возрастом.";
        int age1 = 30;

        //when
        String actual = Helpers.getAllByAge(age, test);
        String actual1 = Helpers.getAllByAge(age1, test);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
    }


    @Test
    void getAllByAge_ShouldReturnPeopleByTheSameAgeFailed() {

        //given
        String expected = "Dariia Kaptiurova,город Kyiv." + "\n";
        int age = 25;

        //when
        String actual = Helpers.getAllByAge(age, test);

        //then
        assertNotEquals(expected, actual);
    }

    @Test
    void getAllByFirstName_ShouldReturnPeopleByTheSameFirstNamePass() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20, город  Kyiv." + "\n";
        String firstName = "Dariia";
        String expected1 = "В базе данных нет пользователей с указанным именем.";
        String firstName1 = "Andrew";

        //when
        String actual = Helpers.getAllByFirstName(firstName, test);
        String actual1 = Helpers.getAllByFirstName(firstName1, test);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
    }


    @Test
    void getAllByFirstName_ShouldReturnPeopleByTheSameFirstNameFailed() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20.";
        String firstName = "Dariia";

        //when
        String actual = Helpers.getAllByFirstName(firstName, test);

        //then
        assertNotEquals(expected, actual);
    }

    @Test
    void getAllByLastName_ShouldReturnPeopleByTheSameLastNamePass() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20, город Kyiv." + "\n";
        String lastName = "Kaptiurova";
        String expected1 = "В базе данных нет пользователем с указанной фамилией.";
        String lastName1 = "Pikozh";


        //when
        String actual = Helpers.getAllByLastName(lastName, test);
        String actual1 = Helpers.getAllByLastName(lastName1, test);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
    }


    @Test
    void getAllByLastName_ShouldReturnPeopleByTheSameLastNameFailed() {

        //given
        String expected = "Dariia Kaptiurova, возраст 20.";
        String lastName = "Kaptiurova";

        //when
        String actual = Helpers.getAllByLastName(lastName, test);

        //then
        assertNotEquals(expected, actual);
    }

    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
    }
}